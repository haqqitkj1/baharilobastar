package com.example.androidbaharilobastar;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LoginActivity extends AppCompatActivity {
    EditText edtUsername, edtPassword;
    Button btnLogin;
    private static final int CODE_GET_REQUEST  = 1024;
    private static final int CODE_POST_REQUEST = 1025;
    List<Login> loginList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtUsername = findViewById(R.id.username);
        edtPassword = findViewById(R.id.password);

        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginLobster();
            }
        });
    }

    private void loginLobster() {
        String username = edtUsername.getText().toString();
        String password = edtPassword.getText().toString();

        if (TextUtils.isEmpty(username)){
            edtUsername.setError("Masukkan Username");
            edtUsername.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(password)){
            edtPassword.setError("Masukkan Password");
            edtPassword.requestFocus();
            return;
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("username", username);
        params.put("password", password);

        PerformNetworkRequest request = new PerformNetworkRequest(Api.URL_LOGIN, params, CODE_POST_REQUEST);
        request.execute();
    }

    private class PerformNetworkRequest extends AsyncTask<Void, Void, String>{
        String url;
        HashMap<String, String> params;
        int requestCode;

        PerformNetworkRequest(String url, HashMap<String, String> params, int requestCode){
            this.url = url;
            this.params = params;
            this.requestCode = requestCode;
        }

        @Override
        protected String doInBackground(Void... voids) {
            RequestHandler requestHandler = new RequestHandler();
            if (requestCode == CODE_POST_REQUEST) return requestHandler.sendPostRequest(url, params);
            if (requestCode == CODE_GET_REQUEST) return requestHandler.sendGetRequest(url);
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject object = new JSONObject(s);
                if (!object.getBoolean("error")){
                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_LONG).show();
                    Intent in = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(in);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
