package com.example.androidbaharilobastar;

class Lobster {
    private int id;
    private String jenis, tipe;
    private int stok, harga;

    public Lobster(int id, String jenis, String tipe, int stok, int harga) {
        this.id = id;
        this.jenis = jenis;
        this.tipe = tipe;
        this.stok = stok;
        this.harga = harga;
    }

    public int getId() {
        return id;
    }

    public String getJenis() {
        return jenis;
    }

    public String getTipe() {
        return tipe;
    }

    public int getStok() {
        return stok;
    }

    public int getHarga() {
        return harga;
    }
}
