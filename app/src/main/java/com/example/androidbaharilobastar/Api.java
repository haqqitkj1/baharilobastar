package com.example.androidbaharilobastar;

public class Api {

    private static final String ROOT_URL = "http://192.168.43.112/BahariApi/v1/Api.php?apicall=";

    public static final String URL_LOGIN = ROOT_URL + "getlogin";


    public static final String URL_CREATE_LOBSTER = ROOT_URL + "createlobster";
    public static final String URL_READ_LOBSTER = ROOT_URL + "getlobster";
    public static final String URL_UPDATE_LOBSTER = ROOT_URL + "updatelobster";
    public static final String URL_DELETE_LOBSTER = ROOT_URL + "deletelobster&id=";

}
