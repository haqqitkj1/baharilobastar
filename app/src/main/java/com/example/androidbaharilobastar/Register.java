package com.example.androidbaharilobastar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class Register extends AppCompatActivity {
    EditText usrId, usrUsername, usrPassword;
    Spinner spinPermission;
    Button btnRegist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        usrId = (EditText)findViewById(R.id.userId);
        usrUsername = (EditText)findViewById(R.id.userUsername);
        usrPassword = (EditText)findViewById(R.id.userPassword);
        spinPermission = (Spinner)findViewById(R.id.spinnerPermission);

        btnRegist = (Button)findViewById(R.id.btnUserRegister);
        btnRegist.setOnClickListener((view) -> {
            createUser();
        });
    }

    private void createUser() {
    }
}
