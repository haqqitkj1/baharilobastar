package com.example.androidbaharilobastar;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.view.View.GONE;

public class MainActivity extends AppCompatActivity {

    private static final int CODE_GET_REQUEST = 1024;
    private static final int CODE_POST_REQUEST = 1025;

    EditText edtId, edtJenis, edtHarga;
    SeekBar stokBar;
    Spinner spinnerTipe;
    ProgressBar progressBar;
    ListView listView;
    Button buttonAddUpdate;

    List<Lobster> lobsterList = new ArrayList<>();

    boolean isUpdating = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtId = (EditText)findViewById(R.id.editTextLobsterId);
        edtJenis = (EditText)findViewById(R.id.editTextJenis);
        edtHarga = (EditText)findViewById(R.id.editTextHarga);

        stokBar = (SeekBar)findViewById(R.id.stokBar);
        stokBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedValue = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText(MainActivity.this, "Stok Lobster adalah : " + progressChangedValue + " kg",
                        Toast.LENGTH_SHORT).show();
            }
        });

        spinnerTipe = (Spinner)findViewById(R.id.spinnerTipe);

        buttonAddUpdate = (Button)findViewById(R.id.buttonAddUpdate);

        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        listView = (ListView)findViewById(R.id.listViewLobster);

        buttonAddUpdate.setOnClickListener((view) -> {
            if (isUpdating){
                    updateLobster();
            } else {
                createLobster();
            }
        });
        readLobster();
    }

    private void createLobster() {
        String jenis = edtJenis.getText().toString().trim();
        String tipe = spinnerTipe.getSelectedItem().toString();
        int stok = stokBar.getProgress();
        int harga = (Integer.parseInt(edtHarga.getText().toString().trim()));

        if (TextUtils.isEmpty(jenis)){
            edtJenis.setError("Masukkan Jenis Lobster");
            edtJenis.requestFocus();
            return;
        }

        if (harga == 0){
            edtHarga.setError("Masukkan Harga Lobster");
            edtHarga.requestFocus();
            return;
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("jenis", jenis);
        params.put("tipe", tipe);
        params.put("stok", String.valueOf(stok));
        params.put("harga", String.valueOf(harga));

        PerformNetworkRequest request = new PerformNetworkRequest(Api.URL_CREATE_LOBSTER, params, CODE_POST_REQUEST);
        request.execute();
    }

    private void deleteLobster(int id) {
        PerformNetworkRequest request = new PerformNetworkRequest(Api.URL_DELETE_LOBSTER + id, null, CODE_GET_REQUEST);
        request.execute();
    }

    private void readLobster() {
        PerformNetworkRequest request = new PerformNetworkRequest(Api.URL_READ_LOBSTER, null, CODE_GET_REQUEST);
        request.execute();
    }

    private void updateLobster() {
        String id = edtId.getText().toString();
        String jenis = edtJenis.getText().toString().trim();
        String tipe = spinnerTipe.getSelectedItem().toString();
        int stok = stokBar.getProgress();
        int harga = (Integer.parseInt(edtHarga.getText().toString().trim()));


        if (TextUtils.isEmpty(jenis)){
            edtJenis.setError("Masukkan Jenis Lobster");
            edtJenis.requestFocus();
            return;
        }

        if (harga == 0){
            edtHarga.setError("Masukkan Harga Lobster");
            edtHarga.requestFocus();
            return;
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("id", id);
        params.put("jenis", jenis);
        params.put("tipe", tipe);
        params.put("stok", String.valueOf(stok));
        params.put("harga", String.valueOf(harga));


        PerformNetworkRequest request = new PerformNetworkRequest(Api.URL_UPDATE_LOBSTER, params, CODE_POST_REQUEST);
        request.execute();

        buttonAddUpdate.setText("Add");

        edtJenis.setText("");
        spinnerTipe.setSelection(0);
        stokBar.setProgress(0);
        edtHarga.setText("");

        isUpdating = false;

    }

    private void refreshLobsterList(JSONArray lobster) throws JSONException {
        lobsterList.clear();

        for (int i = 0; i < lobster.length(); i++) {
            JSONObject obj = lobster.getJSONObject(i);

            lobsterList.add(new Lobster(
                    obj.getInt("id"),
                    obj.getString("jenis"),
                    obj.getString("tipe"),
                    obj.getInt("stok"),
                    obj.getInt("harga")
            ));
        }

        LobsterAdapter adapter = new LobsterAdapter(lobsterList);
        listView.setAdapter(adapter);
    }

    private class PerformNetworkRequest extends AsyncTask<Void, Void, String> {
        String url;
        HashMap<String, String> params;
        int requestCode;

        PerformNetworkRequest(String url, HashMap<String, String> params, int requestCode) {
            this.url = url;
            this.params = params;
            this.requestCode = requestCode;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressBar.setVisibility(GONE);
            try {
                JSONObject object = new JSONObject(s);
                if (!object.getBoolean("error")) {
                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                    refreshLobsterList(object.getJSONArray("lobster"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(Void... voids) {
            RequestHandler requestHandler = new RequestHandler();

            if (requestCode == CODE_POST_REQUEST)
                return requestHandler.sendPostRequest(url, params);


            if (requestCode == CODE_GET_REQUEST)
                return requestHandler.sendGetRequest(url);
            return null;
        }
    }

    class LobsterAdapter extends ArrayAdapter<Lobster> {
        List<Lobster> lobsterList;

        public LobsterAdapter(List<Lobster> lobsterList){
            super(MainActivity.this, R.layout.layout_lobster_list, lobsterList);
            this.lobsterList = lobsterList;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View listViewItem = inflater.inflate(R.layout.layout_lobster_list, null, true);

            TextView textViewLJenis = listViewItem.findViewById(R.id.textViewListJenis);
            TextView textViewLStok = listViewItem.findViewById(R.id.textViewListStok);

            TextView textViewUpdate = listViewItem.findViewById(R.id.textViewUpdate);
            TextView textViewDelete = listViewItem.findViewById(R.id.textViewDelete);

            final Lobster lobster = lobsterList.get(position);

            textViewLJenis.setText(lobster.getJenis());
            textViewLStok.setText(lobster.getStok()+" kg");

            textViewUpdate.setOnClickListener((view) -> {
                isUpdating = true;
                edtId.setText(String.valueOf(lobster.getId()));
                edtJenis.setText(lobster.getJenis());
                spinnerTipe.setSelection(((ArrayAdapter<String>) spinnerTipe.getAdapter()).getPosition(lobster.getTipe()));
                stokBar.setProgress(lobster.getStok());
                edtHarga.setText(String.valueOf(lobster.getHarga()));
                buttonAddUpdate.setText("Update");
            });

            textViewDelete.setOnClickListener((view) -> {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                builder.setTitle("Delete " + lobster.getJenis())
                .setMessage("Are you sure want to delete it?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                deleteLobster(lobster.getId());
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).setIcon(android.R.drawable.ic_dialog_alert).show();
            });

            return listViewItem;
        }
    }
}
