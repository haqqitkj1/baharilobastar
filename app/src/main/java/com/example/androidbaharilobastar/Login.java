package com.example.androidbaharilobastar;

public class Login {
    private String username, password;
    private int id, permission;

    public Login(int id,String username, String password, int permission) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getId() {
        return id;
    }

    public int getPermission() {
        return permission;
    }
}
